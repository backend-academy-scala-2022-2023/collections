import scala.collection.immutable.TreeMap
import scala.io.Source
import scala.util.Using
import scala.util.matching.Regex

@main def collections: Unit = {
  var s = List(1, 2, 5, 1)
  s = s :+ 3
  println(0 +: s)
//  (0 to 10000000).foreach { i =>
//    s :+ 1
//  }

  var v = Vector(1, 2, 5, 1)
//  (0 to 10000000).foreach { i =>
//    v = v :+ 1
//  }
  var t: Seq[Int] = Vector(1, 2, 5, 1)
  val y: Seq[Int] = 0.to(10000000)

  val evenNumbers = Set(2, 4, 6, 8)
  println(evenNumbers)
  println(evenNumbers + 10)

  val m = Map(
    "Golikova" -> 49,
    "Golikov" -> 26
  )

  "Golikov".->(26)
  println(m + ("K" -> 8))
  println(TreeMap.from(m))
  println(Seq.empty)

  (1 to 100).foreach { i =>
    if (i % 33 == 0)
      println(i)
  }
  println(Set(1, 3, 5).map(3 * _))
  val oneToFive = 1 to 5
  println(oneToFive.toList.flatMap(i => List.fill(i)(i)))
  println(oneToFive.filter(_ % 2 == 0))
  println(oneToFive.filterNot(_ % 2 == 0))
  println(oneToFive.collect {
    case i if i % 2 == 0 => i * 3
  })
  println(oneToFive.filter(_ % 2 == 0).map(_ * 3))
  println(oneToFive.find(_ % 2 == 0))
  println(oneToFive.findLast(_ % 2 == 0))
  println(oneToFive.contains(6))
  println(oneToFive.exists(_ % 7 == 0))
  println(oneToFive.forall(_ < 7))

  println(oneToFive.groupBy(_ % 3))
  println(oneToFive.sortBy(_ % 3))
  println(oneToFive.toList.reverse)
  println(oneToFive.toList.reverse.sorted)
  println(oneToFive.zip(oneToFive.reverse))

  println(oneToFive.reduce { case (acc, v) => acc * v })
  println(Seq.empty[Int].reduceOption { case (acc, v) => acc * v })
  println(Seq(4, 6, 12, 7, 10).foldLeft(-1) {
    case (max, v) if (v > max)  => v
    case (max, v) if (v <= max) => max
  })
  Seq(4, 6, 12, 7, 10).max
  println(
    oneToFive.toList
      .filter(_ % 2 == 0)
      .flatMap(i =>
        List
          .range(0, i)
          .map(_ * i)
          .flatMap(s => List.fill(s)(i))
      )
  )

  for {
    i <- oneToFive.toList
    if i % 2 == 0
    r <- List.range(0, i)
    s = r * i
    s <- List.fill(s)(i)
  } yield s

  val lines = Using(Source.fromFile("my-file.txt")) { file =>
    file.getLines().toSeq
  }

  lines.foreach(l =>
    l.zipWithIndex
      .foreach(println)
  )

  val regexp: Regex = "([\\w\\s]+) is ([\\w\\s]+)".r
  println(regexp.matches("My name is Slim Shady"))
  println(regexp.matches("Hello"))
  println(List(1, 2, 3) match
    case List(_, y, _) => y
  )
  println("My name is Slim Shady" match
    case regexp(term, explanation) => term + " is actually " + explanation
  )
}
