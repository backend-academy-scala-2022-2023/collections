trait GreatHouse {
  def name: String
  def wealth: Wealth
}

trait Wealth {
  def money: Int
  def army: Int
}

case class SimpleWealth(override val money: Int, override val army: Int)
    extends Wealth

trait MakeWildFire {
  this: GreatHouse =>
  def makeWildFare(): Wealth =
    SimpleWealth(wealth.money, wealth.army * 5)
}

trait BorrowMoney {
  this: GreatHouse =>
  def borrowMoney(): Wealth =
    SimpleWealth(wealth.money + 100, wealth.army)
}

trait CallDragon {
  this: GreatHouse =>
  def callDragon(): Wealth =
    SimpleWealth(wealth.money, wealth.army * 2)
}

case class Lannisters(wealth: Wealth)
    extends GreatHouse
    with BorrowMoney
    with MakeWildFire {
  override val name = "Lannisters"
}

case class Targaryen(wealth: Wealth)
    extends GreatHouse
    with BorrowMoney
    with CallDragon {
  override val name = "Targaryen"
}

case class GameOfThrones(
    moveNumber: Int = 0,
    lannisters: Lannisters,
    targaryen: Targaryen
) {

//   {
//     def rate(amount: BigInt)(history: Any): Int = ???
//     def doSomethingWithHistory(action: Any => Int): Int
//     val ourAmount = 100000
//     ourHistory.doSomethingWithHistory(rate(ourAmount))
//     bkiHistory.doSomethingWithHistory(rate(amount))
//   }

  def nextTurn(
      lannistersMove: Lannisters => Wealth
  )(targaryenMove: Targaryen => Wealth): GameOfThrones = {
    GameOfThrones(
      moveNumber + 1,
      lannisters.copy(wealth = lannistersMove(lannisters)),
      targaryen.copy(wealth = targaryenMove(targaryen))
    )
  }
}

@main def game: Unit =
  println(
    GameOfThrones(
      0,
      Lannisters(SimpleWealth(2, 3)),
      Targaryen(SimpleWealth(2, 3))
    )
      .nextTurn(l => l.borrowMoney())(t => t.borrowMoney())
      .nextTurn(l => l.makeWildFare())(t => t.callDragon())
  )
